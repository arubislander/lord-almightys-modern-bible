/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id: page
    title: qsTr("About")
    Column {
        id: column
        anchors.fill: parent
        anchors.margins: 20
        spacing: 5
        Image {
            id: image
            source: "files/images/LAMB.png"
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
        }
        ToolSeparator {
            orientation: Qt.Horizontal
            width: parent.width
            contentItem: Rectangle {
                implicitHeight: 1
            }
        }
        Label {
            id: descriptiontext
            text: "Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application. It was designed with mobile in mind and provides an offline copy of the Bible in an elegant look and feel. The Bible in this app is based on the 2020 edition of the World English Bible. Neither the scripture nor the translations in Lord Almighty's Modern Bible have been modified from the World English Bible, however, there were major modifications made to the footnotes display and some text from the WEB version. While the Bible text and glossary used in this app are in the public domain, this application and its code are released under a GPL v3 license."
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
            width: column.width
        }
        ToolSeparator {
            orientation: Qt.Horizontal
            width: parent.width
            contentItem: Rectangle {
                implicitHeight: 1
            }
        }
        Label {
            id: linklabel
            text: "https://the3dman.gitlab.io"
            rightInset: 0
            leftInset: 0
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label {
            id: copyrightlabel
            text: qsTr("Copyright © 2021 The3DmaN")
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
