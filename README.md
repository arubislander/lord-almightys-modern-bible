# Description

Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application. It was designed with Linux Mobile in mind and provides an offline copy of the Bible in an elegant look and feel. 

This app was developed to spread scripture to the security minded individuals that need or prefer to read the bible offline for various reasons. The hope is that this app will help to bring scripture to many people across the world who may not have access to stable internet, high-end devices or a safe place to read it online.

The Bible text and glossary in this app are based on the 2020 edition of the World English Bible. Neither the scripture nor the translations in Lord Almighty's Modern Bible have been modified from the World English Bible, however, there were major modifications made to the footnotes display and some text from the WEB version. Each footnote has been reviewed for context and placement. They have been put into a more modern and mobile compatable tooltip popup. The word/words/phrase that they apply to have also been underlined. Quotes from other sections of the Bible have also been moved into popups to prevent the reader from having to skip around. This I hope will help the reader gain a better understanding of what they are reading. 

The World English Bible with glossary is a Public Domain (no copyright) modern English translation of the Holy Bible, and can be found at https://worldenglish.bible

While the Bible text and glossary used in this app are in the public domain, this application and its code are copyrighted and released under a GPL v3 license.

# Install

**Manjaro/Arch Mobile (Phosh and Plasma) and Desktop**

- Install from AUR: (https://aur.archlinux.org/packages/lord-almightys-modern-bible-git/)

**Mobian:**
- Download aarch64 deb from [here](https://gitlab.com/The3DmaN/the3dman.gitlab.io/-/tree/master/content/files/deb/lamb) then in a terminal cd into download directory and install with `sudo apt-get install ./LAMB_XX_aarch64` Note: Replace "XX" with package version downloaded

# Screenshots

![Screenshot](img/lightmode1.png "Light Mode Screenshot")

![Screenshot](img/darkmode2.png "Dark Mode Settings")

![Screenshot](img/darkmode3.png "Dark Mode Screenshot")

![Screenshot](img/darkmode1.png "OLED Mode Screenshot")

# Current Books Included

All Old Testament Books (with all footnotes)

All Deuterocanon Books (most but not all footnotes finished)

All New Testament Books (with all footnotes)

# Known Issues

- Plasma Mobile ignores menu buttons (you can swipe to the right to open the menu still). **From what I can tell, this is a possible bug or support issue with Plasma Mobile and not this app**
