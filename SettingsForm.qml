import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1


Page {
    id: settingspage
    title: qsTr("LAMB Settings")


    Column{
        width: parent.width
        height: parent.height
        leftPadding: 10
        rightPadding: 10

        Label {
            topPadding: 10
            bottomPadding: 10
            text: "Theme:"
        }

        ButtonGroup {
            buttons: themerow.children
        }

        Row {
            id: themerow

            RadioButton {
                checked: {
                    if (mainsettings.themesetting === "Light") {
                        true
                    }
                }
                text: qsTr("Light")
                onToggled: mainsettings.themesetting = "Light"
            }
            RadioButton {
                checked: {
                    if (mainsettings.themesetting === "Dark") {
                        true
                    }
                }
                text: qsTr("Dark")
                onToggled: mainsettings.themesetting = "Dark"
            }
            RadioButton {
                checked: {
                    if (mainsettings.themesetting === "OLED") {
                        true
                    }
                }
                text: qsTr("OLED")
                onToggled: mainsettings.themesetting = "OLED"
            }
        }
        Label {
            topPadding: 10
            bottomPadding: 10
            text: "Scripture Text Size:"
        }
        Row {
            id: textsizerow

            RadioButton {
                checked: {
                    if (mainsettings.textsize === "Small") {
                        true
                    }
                }
                text: qsTr("Small")
                onToggled: mainsettings.textsize = "Small"
            }
            RadioButton {
                checked: {
                    if (mainsettings.textsize === "Medium") {
                        true
                    }
                }
                text: qsTr("Medium")
                onToggled: mainsettings.textsize = "Medium"
            }
            RadioButton {
                checked: {
                    if (mainsettings.textsize === "Large") {
                        true
                    }
                }
                text: qsTr("Large")
                onToggled: mainsettings.textsize = "Large"
            }
        }
        Label {
            topPadding: 10
            bottomPadding: 10
            text: "Last Read Location:"
        }
        Button {
            text: qsTr("Reset All Books")
            background: Rectangle {
                radius: 5
                color: {
                    if(mainsettings.themesetting === "Dark" || mainsettings.themesetting === "OLED"){
                        "#545454"
                    } else {
                        "#d6cab9"
                    }
                }
            }
            onClicked: resetbookdialog.open()
        }
        Dialog {
            id: resetbookdialog
            anchors.centerIn: parent
            title: "Are you sure?"
            modal: true
            standardButtons: Dialog.Ok | Dialog.Cancel

            onAccepted: mainsettings.resetbooks = "YES"
            onRejected: mainsettings.resetbooks = "NO"
        }
    }
}
